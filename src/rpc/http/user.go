package main

import (
	"encoding/json"
	"fmt"
	"C:/Users/User PC/Desktop/ProjectsGo/lesson-6/src/domain"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func get_content(url string) domain.Album{
	res, err := http.Get(url)
	if err != nil{
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(res.Body)

	if err != nil{
		panic(err.Error())
	}

	var albumData domain.Album
	json.Unmarshal(body, &albumData)
	return albumData
	os.Exit(0)
}

func main(){
	fmt.Println(get_content("https://jsonplaceholder.typicode.com/users/1/albums"))
}